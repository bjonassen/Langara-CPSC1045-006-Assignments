//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");


function changeRadii() {
    console.log("running");
    let inputR = document.querySelector("#inputR");
    let r = Number(inputR.value);
    let circleLeft = document.querySelector("#circleLeft");
    let circleRight = document.querySelector("#circleRight");
    circleLeft.setAttribute("r", inputR.value);
    circleRight.setAttribute("r", 200- inputR.value);
}
window.changeRadii = changeRadii;


function lengthFunction(){
  let lengthInput=document.querySelector("#hairLength");
  let hairVal = Number(lengthInput.value);
  let hairL=document.querySelector("#hairL");
  let hairR=document.querySelector("#hairR");
  hairL.setAttribute("height", 10+(hairVal*2.5));
  hairR.setAttribute("height", 10+(hairVal*2.5));
  hairBack.setAttribute("height", hairVal*2.5);
}
window.lengthFunction = lengthFunction;

function smileFunction(){
  let smileInput=document.querySelector("#changeSmile");
  let smileVal=Number(smileInput.value);
  let mouthL=document.querySelector("#mouthL");
  let mouthR=document.querySelector("#mouthR");
  mouthL.setAttribute("y2", 340+smileVal);
  mouthR.setAttribute("y2", 340+smileVal);
}
window.smileFunction = smileFunction;

function xEyeFunction(){
  let xPupil=document.querySelector("#xPupil");
  let cx=Number(xPupil.value);
  let rightEye=document.querySelector("#rightEye");
  let leftEye=document.querySelector("#leftEye");
  rightEye.setAttribute("cx", 300+cx);
  leftEye.setAttribute("cx", 200+cx);
}
window.xEyeFunction = xEyeFunction;

function yEyeFunction(){
  let yPupil=document.querySelector("#yPupil");
  let cy=Number(yPupil.value);
  let rightEye=document.querySelector("#rightEye");
  let leftEye=document.querySelector("#leftEye");
  rightEye.setAttribute("cy", 200-cy);
  leftEye.setAttribute("cy", 200-cy);
}
window.yEyeFunction = yEyeFunction;

function colorFunction(){
  let colorInput=document.querySelector("#hairColor");
  let colorOutput=document.querySelector("#hair");
  let hairL=document.querySelector("#hairL");
  let hairR=document.querySelector("#hairR");
  colorOutput.setAttribute("fill", colorInput.value);
  hairL.setAttribute("fill", colorInput.value);
  hairR.setAttribute("fill", colorInput.value);
  hairBack.setAttribute("fill", colorInput.value);
}
window.colorFunction = colorFunction;


