/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");


function changeRadii() {
    console.log("running");
    let inputR = document.querySelector("#inputR");
    let r = Number(inputR.value);
    let circleLeft = document.querySelector("#circleLeft");
    let circleRight = document.querySelector("#circleRight");
    circleLeft.setAttribute("r", inputR.value);
    circleRight.setAttribute("r", 200- inputR.value);
}
window.changeRadii = changeRadii;


function lengthFunction(){
  let lengthInput=document.querySelector("#hairLength");
  let hairVal = Number(lengthInput.value);
  let hairL=document.querySelector("#hairL");
  let hairR=document.querySelector("#hairR");
  hairL.setAttribute("height", 10+(hairVal*2.5));
  hairR.setAttribute("height", 10+(hairVal*2.5));
  hairBack.setAttribute("height", hairVal*2.5);
}
window.lengthFunction = lengthFunction;

function smileFunction(){
  let smileInput=document.querySelector("#changeSmile");
  let smileVal=Number(smileInput.value);
  let mouthL=document.querySelector("#mouthL");
  let mouthR=document.querySelector("#mouthR");
  mouthL.setAttribute("y2", 340+smileVal);
  mouthR.setAttribute("y2", 340+smileVal);
}
window.smileFunction = smileFunction;

function xEyeFunction(){
  let xPupil=document.querySelector("#xPupil");
  let cx=Number(xPupil.value);
  let rightEye=document.querySelector("#rightEye");
  let leftEye=document.querySelector("#leftEye");
  rightEye.setAttribute("cx", 300+cx);
  leftEye.setAttribute("cx", 200+cx);
}
window.xEyeFunction = xEyeFunction;

function yEyeFunction(){
  let yPupil=document.querySelector("#yPupil");
  let cy=Number(yPupil.value);
  let rightEye=document.querySelector("#rightEye");
  let leftEye=document.querySelector("#leftEye");
  rightEye.setAttribute("cy", 200-cy);
  leftEye.setAttribute("cy", 200-cy);
}
window.yEyeFunction = yEyeFunction;

function colorFunction(){
  let colorInput=document.querySelector("#hairColor");
  let colorOutput=document.querySelector("#hair");
  let hairL=document.querySelector("#hairL");
  let hairR=document.querySelector("#hairR");
  colorOutput.setAttribute("fill", colorInput.value);
  hairL.setAttribute("fill", colorInput.value);
  hairR.setAttribute("fill", colorInput.value);
  hairBack.setAttribute("fill", colorInput.value);
}
window.colorFunction = colorFunction;




/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiLy9UaGlzIGlzIHRoZSBlbnRyeSBwb2ludCBKYXZhU2NyaXB0IGZpbGUuXHJcbi8vV2VicGFjayB3aWxsIGxvb2sgYXQgdGhpcyBmaWxlIGZpcnN0LCBhbmQgdGhlbiBjaGVja1xyXG4vL3doYXQgZmlsZXMgYXJlIGxpbmtlZCB0byBpdC5cclxuY29uc29sZS5sb2coXCJIZWxsbyB3b3JsZFwiKTtcclxuXHJcblxyXG5mdW5jdGlvbiBjaGFuZ2VSYWRpaSgpIHtcclxuICAgIGNvbnNvbGUubG9nKFwicnVubmluZ1wiKTtcclxuICAgIGxldCBpbnB1dFIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2lucHV0UlwiKTtcclxuICAgIGxldCByID0gTnVtYmVyKGlucHV0Ui52YWx1ZSk7XHJcbiAgICBsZXQgY2lyY2xlTGVmdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjY2lyY2xlTGVmdFwiKTtcclxuICAgIGxldCBjaXJjbGVSaWdodCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjY2lyY2xlUmlnaHRcIik7XHJcbiAgICBjaXJjbGVMZWZ0LnNldEF0dHJpYnV0ZShcInJcIiwgaW5wdXRSLnZhbHVlKTtcclxuICAgIGNpcmNsZVJpZ2h0LnNldEF0dHJpYnV0ZShcInJcIiwgMjAwLSBpbnB1dFIudmFsdWUpO1xyXG59XHJcbndpbmRvdy5jaGFuZ2VSYWRpaSA9IGNoYW5nZVJhZGlpO1xyXG5cclxuXHJcbmZ1bmN0aW9uIGxlbmd0aEZ1bmN0aW9uKCl7XHJcbiAgbGV0IGxlbmd0aElucHV0PWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjaGFpckxlbmd0aFwiKTtcclxuICBsZXQgaGFpclZhbCA9IE51bWJlcihsZW5ndGhJbnB1dC52YWx1ZSk7XHJcbiAgbGV0IGhhaXJMPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjaGFpckxcIik7XHJcbiAgbGV0IGhhaXJSPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjaGFpclJcIik7XHJcbiAgaGFpckwuc2V0QXR0cmlidXRlKFwiaGVpZ2h0XCIsIDEwKyhoYWlyVmFsKjIuNSkpO1xyXG4gIGhhaXJSLnNldEF0dHJpYnV0ZShcImhlaWdodFwiLCAxMCsoaGFpclZhbCoyLjUpKTtcclxuICBoYWlyQmFjay5zZXRBdHRyaWJ1dGUoXCJoZWlnaHRcIiwgaGFpclZhbCoyLjUpO1xyXG59XHJcbndpbmRvdy5sZW5ndGhGdW5jdGlvbiA9IGxlbmd0aEZ1bmN0aW9uO1xyXG5cclxuZnVuY3Rpb24gc21pbGVGdW5jdGlvbigpe1xyXG4gIGxldCBzbWlsZUlucHV0PWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjY2hhbmdlU21pbGVcIik7XHJcbiAgbGV0IHNtaWxlVmFsPU51bWJlcihzbWlsZUlucHV0LnZhbHVlKTtcclxuICBsZXQgbW91dGhMPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbW91dGhMXCIpO1xyXG4gIGxldCBtb3V0aFI9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNtb3V0aFJcIik7XHJcbiAgbW91dGhMLnNldEF0dHJpYnV0ZShcInkyXCIsIDM0MCtzbWlsZVZhbCk7XHJcbiAgbW91dGhSLnNldEF0dHJpYnV0ZShcInkyXCIsIDM0MCtzbWlsZVZhbCk7XHJcbn1cclxud2luZG93LnNtaWxlRnVuY3Rpb24gPSBzbWlsZUZ1bmN0aW9uO1xyXG5cclxuZnVuY3Rpb24geEV5ZUZ1bmN0aW9uKCl7XHJcbiAgbGV0IHhQdXBpbD1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3hQdXBpbFwiKTtcclxuICBsZXQgY3g9TnVtYmVyKHhQdXBpbC52YWx1ZSk7XHJcbiAgbGV0IHJpZ2h0RXllPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcmlnaHRFeWVcIik7XHJcbiAgbGV0IGxlZnRFeWU9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNsZWZ0RXllXCIpO1xyXG4gIHJpZ2h0RXllLnNldEF0dHJpYnV0ZShcImN4XCIsIDMwMCtjeCk7XHJcbiAgbGVmdEV5ZS5zZXRBdHRyaWJ1dGUoXCJjeFwiLCAyMDArY3gpO1xyXG59XHJcbndpbmRvdy54RXllRnVuY3Rpb24gPSB4RXllRnVuY3Rpb247XHJcblxyXG5mdW5jdGlvbiB5RXllRnVuY3Rpb24oKXtcclxuICBsZXQgeVB1cGlsPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjeVB1cGlsXCIpO1xyXG4gIGxldCBjeT1OdW1iZXIoeVB1cGlsLnZhbHVlKTtcclxuICBsZXQgcmlnaHRFeWU9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNyaWdodEV5ZVwiKTtcclxuICBsZXQgbGVmdEV5ZT1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2xlZnRFeWVcIik7XHJcbiAgcmlnaHRFeWUuc2V0QXR0cmlidXRlKFwiY3lcIiwgMjAwLWN5KTtcclxuICBsZWZ0RXllLnNldEF0dHJpYnV0ZShcImN5XCIsIDIwMC1jeSk7XHJcbn1cclxud2luZG93LnlFeWVGdW5jdGlvbiA9IHlFeWVGdW5jdGlvbjtcclxuXHJcbmZ1bmN0aW9uIGNvbG9yRnVuY3Rpb24oKXtcclxuICBsZXQgY29sb3JJbnB1dD1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2hhaXJDb2xvclwiKTtcclxuICBsZXQgY29sb3JPdXRwdXQ9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNoYWlyXCIpO1xyXG4gIGxldCBoYWlyTD1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2hhaXJMXCIpO1xyXG4gIGxldCBoYWlyUj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2hhaXJSXCIpO1xyXG4gIGNvbG9yT3V0cHV0LnNldEF0dHJpYnV0ZShcImZpbGxcIiwgY29sb3JJbnB1dC52YWx1ZSk7XHJcbiAgaGFpckwuc2V0QXR0cmlidXRlKFwiZmlsbFwiLCBjb2xvcklucHV0LnZhbHVlKTtcclxuICBoYWlyUi5zZXRBdHRyaWJ1dGUoXCJmaWxsXCIsIGNvbG9ySW5wdXQudmFsdWUpO1xyXG4gIGhhaXJCYWNrLnNldEF0dHJpYnV0ZShcImZpbGxcIiwgY29sb3JJbnB1dC52YWx1ZSk7XHJcbn1cclxud2luZG93LmNvbG9yRnVuY3Rpb24gPSBjb2xvckZ1bmN0aW9uO1xyXG5cclxuXHJcbiJdLCJzb3VyY2VSb290IjoiIn0=