/*jshint esversion: 6 */

console.log("JS file is connected");

//global variables
let width = 10;
let height = 20;
let gameArea = buildGird(width, height);
let size = 25;
let player = { pos:{x:[0,0,1,1], y:[0,1,0,1]}, isRotate: false };
let dropTime;
let seconds = 0;
let time0 = 750;
let time1 = 500;
let time2 = 250;
let points = 0;

/**
*  "O" means it is occupied, " " means it is empty,
*/
/**
* This builds an array for the grid
*/
function buildGird(width, height){

    let array = [];
    for (let row = 0; row < height; row++) {
        array.push([]);
        for (let col = 0; col < width; col++) {
            array[row][col] = " ";
        }
    }
    return array;

}
/**
* This draws the grid
* @param gameArea 2D array of "O" and " " that represents the game
* @param size in pixels for the block
*/
function drawGrid(gameArea, size) {
    let gb = document.querySelector("#grid");
    let gridString = "";
    for (let row = 0; row < gameArea.length; row++) {
        for (let col = 0; col < gameArea[row].length; col++) {
            gridString += "<rect x='" + col * 25 + "' y='" + row * 25 + "' width ='" + size + "' height='" + size + "' fill = 'grey' stroke='white' stroke-width='.25' />";
            }
        }
        gb.innerHTML = gridString;
}

    buildGird(width, height);
    drawGrid(gameArea, size);

//this is drawing whatever is "O"
function drawBlock(){
    let block = document.querySelector("#player");
    let blockString = "";
    for (let row = 0; row < gameArea.length; row++) {
        for (let col = 0; col < gameArea[row].length; col++) {
            if (gameArea[row][col] == "O"){
                blockString += "<rect x='" + col * 25 + "' y='" + row * 25 + "' width ='" + size + "' height='" + size + "' fill='' stroke='white' stroke-width='.25' />";
              }
          }
        }
      block.innerHTML = blockString;
}

//randomly selects a block for drawing and retuns that choice
function newShape(){
  let randomNum = Math.floor(Math.random() * 3);
  let boxArray = ["box0", "box1", "box2"];
  let box = boxArray[randomNum];
  return box;
}
let box = newShape();

/**
* This drops the player block down by changing what should be "O" and " "
*/
function dropBlock(){
  /**
  * This increases the speed of the falling block
  */
  seconds++;
  if(seconds == 20){
    clearInterval(dropTime);
    dropTime = setInterval(dropBlock, time1)
  } else if(seconds == 60){
    clearInterval(dropTime);
    dropTime = setInterval(dropBlock, time2);
  }
  /**
  *  Drops the player block down by changing what should be "O" and " " based on the box
  */
    if (box == "box0"){
      if (player.pos.y[1] == height-1 || player.pos.y[3] == height-1 || gameArea[player.pos.y[1]+1][player.pos.x[1]] == "O" || gameArea[player.pos.y[3]+1][player.pos.x[3]] == "O"){
        startTop();
      } else {
        for(let i=0; i<4; i++){
          player.pos.y[i] += 1;
          if(player.pos.y[i]-1 >= 0){
            gameArea[player.pos.y[i]-1][player.pos.x[i]] = " ";
          }
        }
        for (let i=0; i<4; i++){
          gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }
    }
  } else if (box == "box1"){
      if (player.isRotate == false){
        if (player.pos.y[1] == height-1 || gameArea[player.pos.y[1] + 1][player.pos.x[1]] == "O"){
            startTop();

          }
          else {
            for(let i=0; i<2; i++){
              player.pos.y[i] += 1;
              if (player.pos.y[i]-1 >= 0){
                gameArea[player.pos.y[i]-1][player.pos.x[i]] = " ";
              }
            }
            for(let i=0; i<2; i++){
                gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
            }
          }
        }
        else { //it is rotated
          if (player.pos.y[1] == height-1 || player.pos.y[0] == height-1 || gameArea[player.pos.y[1] + 1][player.pos.x[1]] == "O" || gameArea[player.pos.y[0] + 1][player.pos.x[0]] == "O"){
            startTop();
          }
         else {
          for(let i=0; i<2; i++){
            player.pos.y[i] += 1;
            if (player.pos.y[i]-1 >= 0){
              gameArea[player.pos.y[i]-1][player.pos.x[i]] = " ";
            }
          }
          for(let i=0; i<2; i++){
              gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
          }
        }
      }
    } else if (box == "box2"){
      if (player.pos.y[0] == height -1  || gameArea[player.pos.y[0] + 1][player.pos.x[0]] == "O"){
          startTop();
      } else {
        for(let i=0; i<1; i++){
            player.pos.y[i] += 1;
            if (player.pos.y[i]-1 >= 0){
              gameArea[player.pos.y[i]-1][player.pos.x[i]] = " ";
            }
        }
        for(let i=0; i<1; i++){
            gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }
      }
    }
    drawBlock();
}

/**
* This moves the player
*/
function moveBlock(event){
  /**
  * Moves the player down
  */
  if (event.key == 'ArrowDown' || event.key == 's') {
      if(box == "box0"){
          if (player.pos.y[1] == height-1 || player.pos.y[3] == height-1 || gameArea[player.pos.y[1]+1][player.pos.x[1]] == "O" || gameArea[player.pos.y[3]+1][player.pos.x[3]] == "O"){
            for (let i=0; i<4; i++){
              gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
            }
            startTop();
          } else {
            for (let i=0; i<4; i++){
              player.pos.y[i] +=1;
                gameArea[player.pos.y[i]-1][player.pos.x[i]] = " ";
            }
            for (let i=0; i<4;i++){
                gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
            }
          }
      } else if (box == "box1"){
        if (player.pos.y[1] == height-1 || gameArea[player.pos.y[1]+1][player.pos.x[1]] == "O" || (player.isRotate == true && gameArea[player.pos.y[0]+1][player.pos.x[0]] == "O")){
          for (let i=0; i<2; i++){
            gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
          }
          startTop();
        } else {
          for (let i=0; i<2; i++){
            player.pos.y[i] +=1;
              gameArea[player.pos.y[i]-1][player.pos.x[i]] = " ";
          }
          for (let i=0; i<2;i++){
              gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
          }
        }
      } else if (box == "box2"){
        if (player.pos.y[0] == height-1 || gameArea[player.pos.y[0]+1][player.pos.x[0]] == "O"){
          for (let i=0; i<1; i++){
            gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
          }
          startTop();
        } else {
          for (let i=0; i<1; i++){
            player.pos.y[i] +=1;
              gameArea[player.pos.y[i]-1][player.pos.x[i]] = " ";
          }
          for (let i=0; i<1;i++){
              gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
          }
        }
      }
      /**
      * Moves the player left
      */
  } else if (event.key == 'ArrowLeft' || event.key == 'a') {
    if(box == "box0"){
        if (player.pos.x[0] == 0 || player.pos.y[1] == 0 || gameArea[player.pos.y[0]][player.pos.x[0]-1] == "O" || gameArea[player.pos.y[1]][player.pos.x[1]-1] == "O"){
          for (let i=0; i<4; i++){
            gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
          }
        } else {
          for (let i=0; i<4; i++){
            player.pos.x[i] -=1;
              gameArea[player.pos.y[i]][player.pos.x[i]+1] = " ";
          }
          for (let i=0; i<4;i++){
              gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
          }
        }
    } else if (box == "box1"){
      if (player.pos.x[1] == 0 || gameArea[player.pos.y[1]][player.pos.x[1]-1] == "O" || (player.isRotate == false && gameArea[player.pos.y[0]][player.pos.x[0]-1] == "O")){
        for (let i=0; i<2; i++){
          gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }
      } else {
        for (let i=0; i<2; i++){
          player.pos.x[i] -=1;
            gameArea[player.pos.y[i]][player.pos.x[i]+1] = " ";
        }
        for (let i=0; i<2;i++){
            gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }
      }
    } else if (box == "box2"){
      if (player.pos.x[0] == 0 || gameArea[player.pos.y[0]][player.pos.x[0]-1] == "O"){
        for (let i=0; i<1; i++){
          gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }
      } else {
        for (let i=0; i<1; i++){
          player.pos.x[i] -=1;
            gameArea[player.pos.y[i]][player.pos.x[i]+1] = " ";
        }
        for (let i=0; i<1;i++){
            gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }
      }
    }
    /**
    * Moves the player right
    */
  } else if (event.key == 'ArrowRight' || event.key == 'd') {
    if(box == "box0"){
        if (player.pos.x[2] == width-1 || player.pos.y[3] == width-1 || gameArea[player.pos.y[2]][player.pos.x[2]+1] == "O" || gameArea[player.pos.y[3]][player.pos.x[3]+1] == "O"){
          for (let i=0; i<4; i++){
            gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
          }
        } else {
          for (let i=0; i<4; i++){
            player.pos.x[i] +=1;
              gameArea[player.pos.y[i]][player.pos.x[i]-1] = " ";
          }
          for (let i=0; i<4;i++){
              gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
          }
        }
    } else if (box == "box1"){
      if (player.pos.x[0] == width -1 || gameArea[player.pos.y[1]][player.pos.x[1]+1] == "O" || (player.isRotate == false && gameArea[player.pos.y[0]][player.pos.x[0]+1] == "O")){
        for (let i=0; i<2; i++){
          gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }

      } else {
        for (let i=0; i<2; i++){
          player.pos.x[i] +=1;
            gameArea[player.pos.y[i]][player.pos.x[i]-1] = " ";
        }
        for (let i=0; i<2;i++){
            gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }
      }
    } else if (box == "box2"){
      if (player.pos.x[0] == width-1 || gameArea[player.pos.y[0]][player.pos.x[0]+1] == "O"){
        for (let i=0; i<1; i++){
          gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }
      } else {
        for (let i=0; i<1; i++){
          player.pos.x[i] +=1;
            gameArea[player.pos.y[i]][player.pos.x[i]-1] = " ";
        }
        for (let i=0; i<1;i++){
            gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
        }
      }
    }
    /**
    * Rotates player as box 1 (2 by 1)
    */
  } else if (event.key == 'ArrowUp' || event.key == 'w') {
      if (box == "box1"){
        if (player.isRotate == false){
          //vertical to horizontal
          if (player.pos.x[1] + 1 > width-1 || gameArea[player.pos.y[1]][player.pos.x[1]+1] == "O"){
          }
          else {
            for (let i=0; i<2; i++){
                gameArea[player.pos.y[i]][player.pos.x[i]] = " ";
            }
            player.pos.x[0] = player.pos.x[1] + 1;
            player.pos.y[0] = player.pos.y[1];
            player.isRotate = true;
            for (let i=0; i<2;i++){
                gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
            }
          }
        }
        else {
          //horizontal to vertical
            if (player.pos.y[0] - 1 <= 0 || gameArea[player.pos.y[0]-1][player.pos.x[0]] == "O"){

          }
          else {
            console.log("rotating back");
            for (let i=0; i<2; i++){
                gameArea[player.pos.y[i]][player.pos.x[i]] = " ";
            }
            player.pos.x[0] = player.pos.x[1];
            player.pos.y[0] = player.pos.y[1] -1;
            for (let i=0; i<2; i++){
                gameArea[player.pos.y[i]][player.pos.x[i]] = "O";
            }
            player.isRotate = false;
          }
        }
      }
    }
  drawBlock();
}

/**
* This stars the game
*/
function fun(){

  newShape();
  dropTime = setInterval(dropBlock, time0);


  }

  /**
  * This checks to see if a row should be cleared, adds points, and adds new row
  */
let check;
function checkGrid(){
  let counter = 0;
  for (let row = 0; row < gameArea.length; row++) {
    check = true;
      for (let col = 0; col < gameArea[row].length; col++) {
          if (gameArea[row][col] != "O"){
            check = false;
          }
        }

    if(check == true){
      counter ++;
      gameArea.splice(row,1);
      let newRow = [];
      for (let i = 0; i < width; i++){
        newRow.push(" ");
      }
      gameArea.unshift(newRow);
      }
    }
    if (counter == 1){
      points ++;
    }
    else if (counter > 1){
      points = points + 10;
    }
    document.querySelector("#points").innerHTML = "Points: "+ points;
  }

/**
* This regenerate a new random block on top of the grid
*/
  function startTop(){
    checkGrid();
    player = { pos:{x:[0,0,1,1], y:[0,1,0,1]}, isRotate: false  };
    box = newShape();
  }
