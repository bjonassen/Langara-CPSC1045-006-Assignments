/*jshint esversion: 6 */

console.log("Sprite file is connected");

function drawAnimal(x, y){
  let animal = document.querySelector("#animal");

  let translateString="translate("+x+" "+y+")";
  animal.setAttribute("transform", translateString);

}

function drawFruit(x, y){
  let fruit = document.querySelector("#fruit");

  let x1 = 75+x;

  let translateString="translate("+x1+" "+y+")";
  fruit.setAttribute("transform", translateString);

}

function drawVehicle(x, y){
  let vehicle = document.querySelector("#vehicle");

  let x1 = 150+x;

  let translateString="translate("+x1+" "+y+")";
  vehicle.setAttribute("transform", translateString);

}

function drawPlant(x, y){
  let plant = document.querySelector("#plant");

  let x1 = 225+x;

  let translateString="translate("+x1+" "+y+")";
  plant.setAttribute("transform", translateString);

}

function fun(){
  let xTarget = event.offsetX;
  let yTarget = event.offsetY;

  let x = Number(xTarget);
  let y = Number(yTarget);

  drawAnimal(x, y);
  drawFruit(x, y);
  drawVehicle(x, y);
  drawPlant(x, y);

}

window.fun=fun;

console.log(drawAnimal(100,100));
console.log(drawFruit(100,100));
console.log(drawVehicle(100,100));
console.log(drawPlant(100,100));
