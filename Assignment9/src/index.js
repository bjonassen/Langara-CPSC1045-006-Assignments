/*jshint esversion: 6 */

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function fun(){
    let dictionary = [
    {word:"dog", definition:"A person regarded as unpleasant, contemptible, or wicked."},{word:"cat", definition:"Ninjas in fur suit with knives hidden in the paws."},
    {word:"door", definition:"A device to walk through walls."},
    {word:"windows", definition:"A PC based operating system by Microsoft."},
    {word:"car", definition:"2-ton steel carriages powered by explosions, made out of dinosaurs which are used for transportation."}
    ];
    var i;
    let input = document.querySelector("#input").value;
    let output = document.querySelector("#output");
    for(i=0; i<dictionary.length; i++){
        if(input == dictionary[i].word){
            output.innerHTML = dictionary[i].definition;
            break;
        }
        else{
            output.innerHTML = "Not found";
        }
    }
}

window.fun=fun;