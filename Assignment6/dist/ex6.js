

let baseString = "TestPage"
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers

    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
    

    var count;
    let count1=0;

    function fun1(){
        upC = containers[count].innerHTML.toUpperCase();
        return upC;
    }

    for (count=0; count<=7; count++){
         count1 +=containers[count].innerHTML.length;
         containers[count].innerHTML = fun1(count);
    }


    

    //Output Part 1: You do not need to change this.
    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count1;

    //Part 2: Rewrite the following using a loop and a function
    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";//start with an empy string
    let x = 0;

    function fun2(){
        pointString = pointString + x + "," + y + " ";
        return pointString;
    }

    for (i=0; i<7; i++){
        x = x + 10;
        y = 200 - heights[i] * 20;
        pointString = fun2(x, y);
    }

    //Replace the following code with a for/of loop.
    //In this loop, call a function that you will write to 
    //help create the string of points

    //x = x + 10;
    //y = 200 - heights[0] * 20;
    //Take the next line of code and make it a function
    //You function will take in, x, y, and the existing string ( 3 parameters into total)
    //and return the new string with the ponts added.
    //Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions
    //pointString = pointString + x + "," + y + " ";

    let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);


    let newThings = []
    //Part 3:  Creating things with loop
    // on the html page with loop.
    //mananually, there are other ways of course.
    let data = [30, 70, 110, 150];
    let svgString = '<svg width="500" height="500">';
    cx= 0;
    cy= 250;
    r= 20;
    fillC="black";

    function fun3(){
        circleString = '<circle cx="'+cx+'"'+' cy="'+cy+'" r="'+r+'" fill="'+fillC+'" />';
        svgString += circleString;
    return svgString;
}

    for(j=0; j<=3; j++){
        cx = data[j];
        svgString = fun3(cx, cy, r, fillC);
    }

    svgString += "</svg>"
    let output3 = document.querySelector("#output3")
    document.body.innerHTML += svgString;

}



