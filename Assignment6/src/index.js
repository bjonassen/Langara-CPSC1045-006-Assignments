//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

  function geneBox(x, y, sizeBox, growthBox, svgString){
    boxString = '<rect x="'+x+'"'+' y="'+100+'" width="'+(sizeBox+y)+'"'+' height="'+(sizeBox+y)+'" fill="black" />';
    svgString += boxString;
    return svgString;
}

function myFunction(){

  let num=document.querySelector("#num");
  let size=document.querySelector("#size");
  let growth=document.querySelector("#growth");

  let numBox=Number(num.value);
  let sizeBox=Number(size.value);
  let growthBox=Number(growth.value);

  let svgString = '<svg width="12000" height="12000">';
  let x=10;
  let y=0;
  for (i=0; i<numBox; i++){
    svgString = geneBox(x, y, sizeBox, growthBox, svgString);
        y +=growthBox;
        x +=((2*y)+sizeBox);
  }

  svgString += "</svg>";
  let output= document.querySelector("#output");
  output.innerHTML = svgString;

}

window.myFunction=myFunction;

