//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function update(){
  let xInput=document.querySelector("#xInput");
  let yInput=document.querySelector("#yInput");
  let rotation=document.querySelector("#rotation");
  let collection=document.querySelector("#collection")
  
  let x=Number(xInput.value);
  let y=Number(yInput.value);
  let r=Number(rotation.value);

  let translateString="translate("+x+" "+y+")";
  let rotateString="rotate("+r+" 100 100)";

  let transformString = translateString +" "+ rotateString;
  collection.setAttribute("transform", transformString);
}
window.update=update;

function update2(){
 let xInput2=document.querySelector("#xInput2");
 let yInput2=document.querySelector("#yInput2");
 let rotate=document.querySelector("#rotate");
 let g=document.querySelector("#g");

 let x2=Number(xInput2.value);
 let y2=Number(yInput2.value);
 let r2=Number(rotate.value);

 let translateString2="translate("+x2+" "+y2+")";
 let rotateString2="rotate("+r2+" 250 250)";
 let transformString2 = translateString2 +" "+ rotateString2;
 g.setAttribute("transform", transformString2);
}
window.update2=update2;

function update1(){
 let innerR=document.querySelector("#innerR");
 let outerR=document.querySelector("#outerR");
 let polygon=document.querySelector("#polygon");

 let iR=Number(innerR.value);
 let oR=Number(outerR.value);

 let p1x=250+(oR*Math.cos(0));
 let p1y=250+(oR*Math.sin(0));
 let p2x=250+(iR*Math.cos(0.76));
 let p2y=250+(iR*Math.sin(0.76));
 let p3x=250+(oR*Math.cos(1.57));
 let p3y=250+(oR*Math.sin(1.57));
 let p4x=250+(iR*Math.cos(2.36));
 let p4y=250+(iR*Math.sin(2.36));
 let p5x=250+(oR*Math.cos(3.14));
 let p5y=250+(oR*Math.sin(3.14));
 let p6x=250+(iR*Math.cos(3.92));
 let p6y=250+(iR*Math.sin(3.92));
 let p7x=250+(oR*Math.cos(4.71));
 let p7y=250+(oR*Math.sin(4.71));
 let p8x=250+(iR*Math.cos(5.50));
 let p8y=250+(iR*Math.sin(5.50));
 let tS=p1x+" "+p1y+" "+p2x+" "+p2y+" "+p3x+" "+p3y+" "+p4x+" "+p4y+" "+p5x+" "+p5y+" "+p6x+" "+p6y+" "+p7x+" "+p7y+" "+p8x+" "+p8y+" ";

 polygon.setAttribute("points", tS);

}
window.update1=update1;
