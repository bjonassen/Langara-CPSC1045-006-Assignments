/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function update(){
  let xInput=document.querySelector("#xInput");
  let yInput=document.querySelector("#yInput");
  let rotation=document.querySelector("#rotation");
  let collection=document.querySelector("#collection")
  
  let x=Number(xInput.value);
  let y=Number(yInput.value);
  let r=Number(rotation.value);

  let translateString="translate("+x+" "+y+")";
  let rotateString="rotate("+r+" 100 100)";

  let transformString = translateString +" "+ rotateString;
  collection.setAttribute("transform", transformString);
}
window.update=update;

function update2(){
 let xInput2=document.querySelector("#xInput2");
 let yInput2=document.querySelector("#yInput2");
 let rotate=document.querySelector("#rotate");
 let g=document.querySelector("#g");

 let x2=Number(xInput2.value);
 let y2=Number(yInput2.value);
 let r2=Number(rotate.value);

 let translateString2="translate("+x2+" "+y2+")";
 let rotateString2="rotate("+r2+" 250 250)";
 let transformString2 = translateString2 +" "+ rotateString2;
 g.setAttribute("transform", transformString2);
}
window.update2=update2;

function update1(){
 let innerR=document.querySelector("#innerR");
 let outerR=document.querySelector("#outerR");
 let polygon=document.querySelector("#polygon");

 let iR=Number(innerR.value);
 let oR=Number(outerR.value);

 let p1x=250+(oR*Math.cos(0));
 let p1y=250+(oR*Math.sin(0));
 let p2x=250+(iR*Math.cos(0.76));
 let p2y=250+(iR*Math.sin(0.76));
 let p3x=250+(oR*Math.cos(1.57));
 let p3y=250+(oR*Math.sin(1.57));
 let p4x=250+(iR*Math.cos(2.36));
 let p4y=250+(iR*Math.sin(2.36));
 let p5x=250+(oR*Math.cos(3.14));
 let p5y=250+(oR*Math.sin(3.14));
 let p6x=250+(iR*Math.cos(3.92));
 let p6y=250+(iR*Math.sin(3.92));
 let p7x=250+(oR*Math.cos(4.71));
 let p7y=250+(oR*Math.sin(4.71));
 let p8x=250+(iR*Math.cos(5.50));
 let p8y=250+(iR*Math.sin(5.50));
 let tS=p1x+" "+p1y+" "+p2x+" "+p2y+" "+p3x+" "+p3y+" "+p4x+" "+p4y+" "+p5x+" "+p5y+" "+p6x+" "+p6y+" "+p7x+" "+p7y+" "+p8x+" "+p8y+" ";

 polygon.setAttribute("points", tS);

}
window.update1=update1;


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvaW5kZXguanNcIik7XG4iLCIvL1RoaXMgaXMgdGhlIGVudHJ5IHBvaW50IEphdmFTY3JpcHQgZmlsZS5cclxuLy9XZWJwYWNrIHdpbGwgbG9vayBhdCB0aGlzIGZpbGUgZmlyc3QsIGFuZCB0aGVuIGNoZWNrXHJcbi8vd2hhdCBmaWxlcyBhcmUgbGlua2VkIHRvIGl0LlxyXG5jb25zb2xlLmxvZyhcIkhlbGxvIHdvcmxkXCIpO1xyXG5cclxuZnVuY3Rpb24gdXBkYXRlKCl7XHJcbiAgbGV0IHhJbnB1dD1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3hJbnB1dFwiKTtcclxuICBsZXQgeUlucHV0PWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjeUlucHV0XCIpO1xyXG4gIGxldCByb3RhdGlvbj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3JvdGF0aW9uXCIpO1xyXG4gIGxldCBjb2xsZWN0aW9uPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjY29sbGVjdGlvblwiKVxyXG4gIFxyXG4gIGxldCB4PU51bWJlcih4SW5wdXQudmFsdWUpO1xyXG4gIGxldCB5PU51bWJlcih5SW5wdXQudmFsdWUpO1xyXG4gIGxldCByPU51bWJlcihyb3RhdGlvbi52YWx1ZSk7XHJcblxyXG4gIGxldCB0cmFuc2xhdGVTdHJpbmc9XCJ0cmFuc2xhdGUoXCIreCtcIiBcIit5K1wiKVwiO1xyXG4gIGxldCByb3RhdGVTdHJpbmc9XCJyb3RhdGUoXCIrcitcIiAxMDAgMTAwKVwiO1xyXG5cclxuICBsZXQgdHJhbnNmb3JtU3RyaW5nID0gdHJhbnNsYXRlU3RyaW5nICtcIiBcIisgcm90YXRlU3RyaW5nO1xyXG4gIGNvbGxlY3Rpb24uc2V0QXR0cmlidXRlKFwidHJhbnNmb3JtXCIsIHRyYW5zZm9ybVN0cmluZyk7XHJcbn1cclxud2luZG93LnVwZGF0ZT11cGRhdGU7XHJcblxyXG5mdW5jdGlvbiB1cGRhdGUyKCl7XHJcbiBsZXQgeElucHV0Mj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3hJbnB1dDJcIik7XHJcbiBsZXQgeUlucHV0Mj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3lJbnB1dDJcIik7XHJcbiBsZXQgcm90YXRlPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcm90YXRlXCIpO1xyXG4gbGV0IGc9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNnXCIpO1xyXG5cclxuIGxldCB4Mj1OdW1iZXIoeElucHV0Mi52YWx1ZSk7XHJcbiBsZXQgeTI9TnVtYmVyKHlJbnB1dDIudmFsdWUpO1xyXG4gbGV0IHIyPU51bWJlcihyb3RhdGUudmFsdWUpO1xyXG5cclxuIGxldCB0cmFuc2xhdGVTdHJpbmcyPVwidHJhbnNsYXRlKFwiK3gyK1wiIFwiK3kyK1wiKVwiO1xyXG4gbGV0IHJvdGF0ZVN0cmluZzI9XCJyb3RhdGUoXCIrcjIrXCIgMjUwIDI1MClcIjtcclxuIGxldCB0cmFuc2Zvcm1TdHJpbmcyID0gdHJhbnNsYXRlU3RyaW5nMiArXCIgXCIrIHJvdGF0ZVN0cmluZzI7XHJcbiBnLnNldEF0dHJpYnV0ZShcInRyYW5zZm9ybVwiLCB0cmFuc2Zvcm1TdHJpbmcyKTtcclxufVxyXG53aW5kb3cudXBkYXRlMj11cGRhdGUyO1xyXG5cclxuZnVuY3Rpb24gdXBkYXRlMSgpe1xyXG4gbGV0IGlubmVyUj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2lubmVyUlwiKTtcclxuIGxldCBvdXRlclI9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNvdXRlclJcIik7XHJcbiBsZXQgcG9seWdvbj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3BvbHlnb25cIik7XHJcblxyXG4gbGV0IGlSPU51bWJlcihpbm5lclIudmFsdWUpO1xyXG4gbGV0IG9SPU51bWJlcihvdXRlclIudmFsdWUpO1xyXG5cclxuIGxldCBwMXg9MjUwKyhvUipNYXRoLmNvcygwKSk7XHJcbiBsZXQgcDF5PTI1MCsob1IqTWF0aC5zaW4oMCkpO1xyXG4gbGV0IHAyeD0yNTArKGlSKk1hdGguY29zKDAuNzYpKTtcclxuIGxldCBwMnk9MjUwKyhpUipNYXRoLnNpbigwLjc2KSk7XHJcbiBsZXQgcDN4PTI1MCsob1IqTWF0aC5jb3MoMS41NykpO1xyXG4gbGV0IHAzeT0yNTArKG9SKk1hdGguc2luKDEuNTcpKTtcclxuIGxldCBwNHg9MjUwKyhpUipNYXRoLmNvcygyLjM2KSk7XHJcbiBsZXQgcDR5PTI1MCsoaVIqTWF0aC5zaW4oMi4zNikpO1xyXG4gbGV0IHA1eD0yNTArKG9SKk1hdGguY29zKDMuMTQpKTtcclxuIGxldCBwNXk9MjUwKyhvUipNYXRoLnNpbigzLjE0KSk7XHJcbiBsZXQgcDZ4PTI1MCsoaVIqTWF0aC5jb3MoMy45MikpO1xyXG4gbGV0IHA2eT0yNTArKGlSKk1hdGguc2luKDMuOTIpKTtcclxuIGxldCBwN3g9MjUwKyhvUipNYXRoLmNvcyg0LjcxKSk7XHJcbiBsZXQgcDd5PTI1MCsob1IqTWF0aC5zaW4oNC43MSkpO1xyXG4gbGV0IHA4eD0yNTArKGlSKk1hdGguY29zKDUuNTApKTtcclxuIGxldCBwOHk9MjUwKyhpUipNYXRoLnNpbig1LjUwKSk7XHJcbiBsZXQgdFM9cDF4K1wiIFwiK3AxeStcIiBcIitwMngrXCIgXCIrcDJ5K1wiIFwiK3AzeCtcIiBcIitwM3krXCIgXCIrcDR4K1wiIFwiK3A0eStcIiBcIitwNXgrXCIgXCIrcDV5K1wiIFwiK3A2eCtcIiBcIitwNnkrXCIgXCIrcDd4K1wiIFwiK3A3eStcIiBcIitwOHgrXCIgXCIrcDh5K1wiIFwiO1xyXG5cclxuIHBvbHlnb24uc2V0QXR0cmlidXRlKFwicG9pbnRzXCIsIHRTKTtcclxuXHJcbn1cclxud2luZG93LnVwZGF0ZTE9dXBkYXRlMTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==