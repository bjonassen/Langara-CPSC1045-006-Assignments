//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function myNum(){
    let num=((Math.random()*10)+1);
    let minN=-10;
    let maxN=10;
    if (num > minN && num < maxN){
          return("True")
        }
    console.log("Hello world");
}
window.myNum=myNum;

function myFunction(){
  let input1=document.querySelector("#input1");
  let input2=document.querySelector("#input2");
  let input3=document.querySelector("#input3");
  let input4=document.querySelector("#input4");
  let input5=document.querySelector("#input5");

  let q1=Number(input1.value);
  let q2=Number(input2.value);
  let q3=Number(input3.value);
  let q4=Number(input4.value);
  let q5=Number(input5.value);

  let label1=document.querySelector("#label1");
  let label2=document.querySelector("#label2");
  let label3=document.querySelector("#label3");
  let label4=document.querySelector("#label4");
  let label5=document.querySelector("#label5");
  let numCorrect=document.querySelector("#numCorrect");

  let a1="Incorrect";
  let a2="Incorrect";
  let a3="Incorrect";
  let a4="Incorrect";
  let a5="Incorrect";
  let counter=0;

  if (q1 == 6){
    a1="Correct";
    counter=counter+1;
  }
  if (q2 == 100.67){
    a2="Correct";
    counter=counter+1;
  }
  if (q3 == 0.34){
    a3="Correct";
    counter=counter+1;
  }
  if (q4 == 25.5){
    a4="Correct";
    counter=counter+1;
  }
  if (q5 == 25.6){
    a5="Correct";
    counter=counter+1;
  }

  label1.innerHTML=a1;
  label2.innerHTML=a2;
  label3.innerHTML=a3;
  label4.innerHTML=a4;
  label5.innerHTML=a5;
  numCorrect.innerHTML=counter+"/5";

}
window.myFunction=myFunction;
