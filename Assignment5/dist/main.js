/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function myNum(){
    let num=((Math.random()*10)+1);
    let minN=-10;
    let maxN=10;
    if (num > minN && num < maxN){
          return("True")
        }
    console.log("Hello world");
}
window.myNum=myNum;

function myFunction(){
  let input1=document.querySelector("#input1");
  let input2=document.querySelector("#input2");
  let input3=document.querySelector("#input3");
  let input4=document.querySelector("#input4");
  let input5=document.querySelector("#input5");

  let q1=Number(input1.value);
  let q2=Number(input2.value);
  let q3=Number(input3.value);
  let q4=Number(input4.value);
  let q5=Number(input5.value);

  let label1=document.querySelector("#label1");
  let label2=document.querySelector("#label2");
  let label3=document.querySelector("#label3");
  let label4=document.querySelector("#label4");
  let label5=document.querySelector("#label5");
  let numCorrect=document.querySelector("#numCorrect");

  let a1="Incorrect";
  let a2="Incorrect";
  let a3="Incorrect";
  let a4="Incorrect";
  let a5="Incorrect";
  let counter=0;

  if (q1 == 6){
    a1="Correct";
    counter=counter+1;
  }
  if (q2 == 100.67){
    a2="Correct";
    counter=counter+1;
  }
  if (q3 == 0.34){
    a3="Correct";
    counter=counter+1;
  }
  if (q4 == 25.5){
    a4="Correct";
    counter=counter+1;
  }
  if (q5 == 25.6){
    a5="Correct";
    counter=counter+1;
  }

  label1.innerHTML=a1;
  label2.innerHTML=a2;
  label3.innerHTML=a3;
  label4.innerHTML=a4;
  label5.innerHTML=a5;
  numCorrect.innerHTML=counter+"/5";

}
window.myFunction=myFunction;


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiLy9UaGlzIGlzIHRoZSBlbnRyeSBwb2ludCBKYXZhU2NyaXB0IGZpbGUuXHJcbi8vV2VicGFjayB3aWxsIGxvb2sgYXQgdGhpcyBmaWxlIGZpcnN0LCBhbmQgdGhlbiBjaGVja1xyXG4vL3doYXQgZmlsZXMgYXJlIGxpbmtlZCB0byBpdC5cclxuY29uc29sZS5sb2coXCJIZWxsbyB3b3JsZFwiKTtcclxuXHJcbmZ1bmN0aW9uIG15TnVtKCl7XHJcbiAgICBsZXQgbnVtPSgoTWF0aC5yYW5kb20oKSoxMCkrMSk7XHJcbiAgICBsZXQgbWluTj0tMTA7XHJcbiAgICBsZXQgbWF4Tj0xMDtcclxuICAgIGlmIChudW0gPiBtaW5OICYmIG51bSA8IG1heE4pe1xyXG4gICAgICAgICAgcmV0dXJuKFwiVHJ1ZVwiKVxyXG4gICAgICAgIH1cclxuICAgIGNvbnNvbGUubG9nKFwiSGVsbG8gd29ybGRcIik7XHJcbn1cclxud2luZG93Lm15TnVtPW15TnVtO1xyXG5cclxuZnVuY3Rpb24gbXlGdW5jdGlvbigpe1xyXG4gIGxldCBpbnB1dDE9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNpbnB1dDFcIik7XHJcbiAgbGV0IGlucHV0Mj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2lucHV0MlwiKTtcclxuICBsZXQgaW5wdXQzPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjaW5wdXQzXCIpO1xyXG4gIGxldCBpbnB1dDQ9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNpbnB1dDRcIik7XHJcbiAgbGV0IGlucHV0NT1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2lucHV0NVwiKTtcclxuXHJcbiAgbGV0IHExPU51bWJlcihpbnB1dDEudmFsdWUpO1xyXG4gIGxldCBxMj1OdW1iZXIoaW5wdXQyLnZhbHVlKTtcclxuICBsZXQgcTM9TnVtYmVyKGlucHV0My52YWx1ZSk7XHJcbiAgbGV0IHE0PU51bWJlcihpbnB1dDQudmFsdWUpO1xyXG4gIGxldCBxNT1OdW1iZXIoaW5wdXQ1LnZhbHVlKTtcclxuXHJcbiAgbGV0IGxhYmVsMT1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2xhYmVsMVwiKTtcclxuICBsZXQgbGFiZWwyPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbGFiZWwyXCIpO1xyXG4gIGxldCBsYWJlbDM9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNsYWJlbDNcIik7XHJcbiAgbGV0IGxhYmVsND1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2xhYmVsNFwiKTtcclxuICBsZXQgbGFiZWw1PWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbGFiZWw1XCIpO1xyXG4gIGxldCBudW1Db3JyZWN0PWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbnVtQ29ycmVjdFwiKTtcclxuXHJcbiAgbGV0IGExPVwiSW5jb3JyZWN0XCI7XHJcbiAgbGV0IGEyPVwiSW5jb3JyZWN0XCI7XHJcbiAgbGV0IGEzPVwiSW5jb3JyZWN0XCI7XHJcbiAgbGV0IGE0PVwiSW5jb3JyZWN0XCI7XHJcbiAgbGV0IGE1PVwiSW5jb3JyZWN0XCI7XHJcbiAgbGV0IGNvdW50ZXI9MDtcclxuXHJcbiAgaWYgKHExID09IDYpe1xyXG4gICAgYTE9XCJDb3JyZWN0XCI7XHJcbiAgICBjb3VudGVyPWNvdW50ZXIrMTtcclxuICB9XHJcbiAgaWYgKHEyID09IDEwMC42Nyl7XHJcbiAgICBhMj1cIkNvcnJlY3RcIjtcclxuICAgIGNvdW50ZXI9Y291bnRlcisxO1xyXG4gIH1cclxuICBpZiAocTMgPT0gMC4zNCl7XHJcbiAgICBhMz1cIkNvcnJlY3RcIjtcclxuICAgIGNvdW50ZXI9Y291bnRlcisxO1xyXG4gIH1cclxuICBpZiAocTQgPT0gMjUuNSl7XHJcbiAgICBhND1cIkNvcnJlY3RcIjtcclxuICAgIGNvdW50ZXI9Y291bnRlcisxO1xyXG4gIH1cclxuICBpZiAocTUgPT0gMjUuNil7XHJcbiAgICBhNT1cIkNvcnJlY3RcIjtcclxuICAgIGNvdW50ZXI9Y291bnRlcisxO1xyXG4gIH1cclxuXHJcbiAgbGFiZWwxLmlubmVySFRNTD1hMTtcclxuICBsYWJlbDIuaW5uZXJIVE1MPWEyO1xyXG4gIGxhYmVsMy5pbm5lckhUTUw9YTM7XHJcbiAgbGFiZWw0LmlubmVySFRNTD1hNDtcclxuICBsYWJlbDUuaW5uZXJIVE1MPWE1O1xyXG4gIG51bUNvcnJlY3QuaW5uZXJIVE1MPWNvdW50ZXIrXCIvNVwiO1xyXG5cclxufVxyXG53aW5kb3cubXlGdW5jdGlvbj1teUZ1bmN0aW9uO1xyXG4iXSwic291cmNlUm9vdCI6IiJ9