/*jshint esversion: 6 */

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

// exercise 8

function fun(){
  let svg = document.querySelector("#svg");
  let xTarget = event.offsetX;
  let yTarget = event.offsetY;
  let color = document.querySelector("#color");

  let x = Number(xTarget);
  let y = Number(yTarget);
  let c = color.value;

  svg.innerHTML += genCircle(x, y, c);

}

function genCircle(x, y, c){
  let circle = '<circle cx="'+x+'" cy="'+y+'" r="25" fill="'+c+'"/>';

  return circle;
}

window.fun=fun;

//assigment 8

function function1(){
  let svg1 = document.querySelector("#svg1");
  let polyline = '<polyline points="" stroke="black" stroke-width="3" fill="none"/>';
  svg1.innerHTML += polyline;
}

window.function1=function1;

function function0(){
  let polyline = document.querySelector("polyline:last-of-type");

  let xTarget = event.offsetX;
  let yTarget = event.offsetY;
  let x = Number(xTarget);
  let y = Number(yTarget);

  let points = polyline.getAttribute("points");
  points += " "+x+", "+y+" ";
  polyline.setAttribute("points", points);
}

window.function0=function0;

setInterval(changeColor, 100);
var j =0;
function changeColor(){
  let allP = document.querySelectorAll("polyline");
  let colorList = ["red", "green", "blue", "black", "orange", "silver", "purple", "yellow", "violet"];
  j++;
  if (j == colorList.length){
    j = 0;
  }
 
  var i;
    for(i=0; i<allP.length; i++){
      allP[i].setAttribute("stroke", colorList[j]);
    }
  }

  window.changeColor=changeColor;
