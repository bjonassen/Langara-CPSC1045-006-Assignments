/*jshint esversion: 6 */

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function fun1(nP, oR, iR){

  let array = [];
  let a=0;
  let b=((360/(2*nP))*Math.PI / 180);

    let outerR= {x:250+(oR*Math.cos(a)), y:250+(oR*Math.sin(a))};
    let innerR= {x:250+(iR*Math.cos(b)), y:250+(iR*Math.sin(b))};

        for (let i=0; i<(2*nP); i++){
            if (i%2==0){
                array.push(outerR);
                a +=((360/nP)* Math.PI / 180);
                outerR= {x:250+(oR*Math.cos(a)), y:250+(oR*Math.sin(a))};
            }
            else {
                array.push(innerR);
                b +=((360/(2*nP))*Math.PI / 180)*2;
                innerR= {x:250+(iR*Math.cos(b)), y:250+(iR*Math.sin(b))};
            }
  }

  return array;
}

function fun2(array){
  let string = "";
    for (let i=0; i<(array.length); i++){
        string += array[i].x +" "+ array[i].y+" ";
    }
  return string;
}

function fun3(){

  let numPoints=document.querySelector("#numPoints");
  let nP=Number(numPoints.value);

  let oR=100;  
  let iR=25;  

  let array = fun1(nP, oR, iR);
  let string = fun2(array);

  let polygon=document.querySelector("#polygon");
  polygon.setAttribute("points", string);

}

window.fun3=fun3;
